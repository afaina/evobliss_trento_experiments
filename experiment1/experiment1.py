import time
import sys

sys.path.append('../../EVOBLISS-SOFTWARE/api')
from evobot import EvoBot
from datalogger import DataLogger
from syringe import Syringe
from head import Head

usrMsgLogger = DataLogger()
evobot = EvoBot("/dev/tty.usbmodemfd1211", usrMsgLogger)
head = Head( evobot )
syringe =  Syringe( evobot, 9)
syringe.plungerSetConversion( 1 ) #ml per mm of displacement of plunger
evobot.home()

syringe.plungerMoveToDefaultPos()
while True:
    try:
        head.move( 20, 100 )
        syringe.syringeMove( -30 )
        syringe.plungerPullVol( 5 ) 
        syringe.syringeMove( 0 )
        head.move( 90, 100 )
        syringe.plungerPushVol( 5 )
    except KeyboardInterrupt:
        break

evobot.disconnect()
